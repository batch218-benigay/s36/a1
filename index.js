// dependencies
const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute.js");

// Server setup
const app = express();
const port = 3001;

//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));
// DB connection
mongoose.connect("mongodb+srv://admin:admin@b218-to-do.hkzfkvp.mongodb.net/toDo?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

app.use("/tasks", taskRoute);


app.listen(port, ()=> console.log(`Server running at ${port}`));