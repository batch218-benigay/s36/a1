// this file contains the features/actions/commands

const Task = require("../models/task.js");


module.exports.getAllTask =() => {
	return Task.find({}).then(result => {
		return result;
	})
}

module.exports.createTask = (requestBody) => {
	let newTask = new Task ({
		name : requestBody.name
	})
	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return "Error detected"; //false
		}
		else{
			return task;
		}
	})
};


// "taskId" parameter will serve as storage of ID in our url/link
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {

		if(err){
			console.log(err);
			return false;
		}
		else{
			return removedTask;
		}
	})
};


module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}
		
		result.name = newContent.name;
		return result.save().then((updateTask, saveErr) =>{
			if(saveErr){
				console.log(saveErr);
				return false;
			}
			else{
				return updateTask;
			}
		})
	})
}


// ACTIVITY

module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	})
}

module.exports.updateStatus = (taskId, newStatus) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}
		
		result.status = newStatus.status;
		return result.save().then((updateStatus, saveErr) =>{
			if(saveErr){
				console.log(saveErr);
				return false;
			}
			else{
				return updateStatus;
			}
		})
	})
}