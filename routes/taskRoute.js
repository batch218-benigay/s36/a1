// This document contains all the endpoints for application and also the http methods

const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController.js");


router.get("/viewTasks", (req, res) => {

	//Invokes the "getAllTasks" function from the "taskController.js" file and sends the result back to the client/Postman
	taskController.getAllTask().then(resultFromController => res.send(resultFromController));
});


router.post("/addNewTask", (req, res) => {
	taskController.createTask(req.body).then(result => res.send(result));
})


router.delete("/deleteTask/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(result => res.send(result));
})


router.put("/updateTask/:id", (req, res) =>{
							//will be the basis of which document we'll update(req.params.id)
											//the new doc/content
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})

//ACTIVITY

router.get("/:id", (req, res) =>{
	taskController.getTask(req.params.id).then(result => res.send(result));
})

router.put("/:id/complete", (req, res) => {
	taskController.updateStatus(req.params.id, req.body).then(result => res.send(result));
})

module.exports = router;